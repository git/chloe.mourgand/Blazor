﻿using MyFirstBlazor.Models;
namespace MyFirstBlazor.Components
{
    public class CraftingRecipe
    {
        public Item Give { get; set; }
        public List<List<string>> Have { get; set; }
    }
}
