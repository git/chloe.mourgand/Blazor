﻿using Microsoft.AspNetCore.Components;
using MyFirstBlazor.Components;
using MyFirstBlazor.Models;
using MyFirstBlazor.Services;

namespace MyFirstBlazor.Pages
{
    public partial class Index
    {
        [Inject]
        public IDataService DataService { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();
            if(firstRender) StateHasChanged();
            await base.OnAfterRenderAsync(firstRender);
        }
        public List<Cake> Cakes { get; set; }

        //protected override Task OnAfterRenderAsync(bool firstRender)
        //{
        //    LoadCakes();
        //    StateHasChanged();
        //    return base.OnAfterRenderAsync(firstRender);
        //}

        public void LoadCakes()
        {
            Cakes = new List<Cake>
        {
            // items hidden for display purpose
            new Cake
            {
                Id = 1,
                Name = "Red Velvet",
                Cost = 60
            },
        };
        }
    }
}
